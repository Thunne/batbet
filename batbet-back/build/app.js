"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// lib/app.ts
var express = require("express");
// Create a new express application instance
var app = express();
app.get('/', function (req, res) {
    res.send('Hello World!');
});
app.get('/lol', function (req, res) {
    res.send('Yo');
});
app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});
